<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head>
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	
	<?php if (is_search()) { ?>
	   <meta name="robots" content="noindex, nofollow" /> 
	<?php } ?>

	<title>
		   <?php
		      if (function_exists('is_tag') && is_tag()) {
		         single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
		      elseif (is_archive()) {
		         wp_title(''); echo ' Archive - '; }
		      elseif (is_search()) {
		         echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
		      elseif (!(is_404()) && (is_single()) || (is_page())) {
		         wp_title(''); echo ' - '; }
		      elseif (is_404()) {
		         echo 'Not Found - '; }
		      if (is_home()) {
		         bloginfo('name'); echo ' - '; bloginfo('description'); }
		      else {
		          bloginfo('name'); }
		      if ($paged>1) {
		         echo ' - page '. $paged; }
		   ?>
	</title>
	
	<!--Google web fonts-->
	<link href='http://fonts.googleapis.com/css?family=Economica:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Galdeano' rel='stylesheet' type='text/css'>	
	<link href='http://fonts.googleapis.com/css?family=Economica:400,700,400italic,700italic|Cabin+Condensed|Acme' rel='stylesheet' type='text/css'>
	<!--End Google web fonts-->
	
	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/images/3dpfavicon64.ico" type="image/x-icon" />
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" />
	<?php wp_head(); ?>
</head>

<body>
	
	<!--Facebook like button script code-->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<!--End script code-->
		
	<div id="header">
		<div id="header-right-container">
			<p id="site-decription">
				<?php bloginfo('description'); ?>
			</p>
			<div id="social-icons">
				<a href="<?php bloginfo('wpurl') ?>/?feed=rss2"><img src="<?php bloginfo('template_directory'); ?>/images/rss_logo.png" alt="Follow 3 Drinks Productions News through RSS" id="button-rss" height="40" width="40" /></a>
				<a href="http://www.facebook.com/3DrinksProductions"><img src="<?php bloginfo('template_directory'); ?>/images/facebook_logo.png" alt="3 Drinks Productions on Facebook" id="button-fb" height="40" width="40" /></a>
				<a href="https://twitter.com/3DrinksProds"><img src="<?php bloginfo('template_directory'); ?>/images/twitter_logo.png" alt="3 Drinks Procudtions on Twitter" id="button-twitter" height="40" width="40" /></a><br /><!--Send like button to next line-->
				<!--Facebook like button-->
				<div class="fb-like" data-href="http://www.facebook.com/3DrinksProductions?ref=ts" data-send="false" data-layout="box_count" data-width="40" data-show-faces="false" data-colorscheme="dark" data-font="verdana"></div>
				<!--End like button-->
			</div> <!--End social icons-->
		</div> <!--End header right-->
		<a href="<?php echo esc_url(get_permalink(get_page_by_title('News'))); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/WatermarkLogo-white.png" alt="3 Drinks Productions" id="header-image" /></a>
		
		<?php wp_nav_menu(
			array(
				'theme_location'=>'main-menu',
				'container_id'=>'navbar',
				'fallback_cb'=>'wp_page_menu'
				)
			); ?>
	</div> <!--End of header-->
	
	<div id="menu-back"></div><!--End menu back-->
	
	<div id="page-container">
