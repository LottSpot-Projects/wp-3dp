<?php 
/*
 * Template Name: Static Page
 */
get_header(); ?>

<?php 
get_sidebar(); ?>

<div class="content">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<?php the_content(); ?>
			
	<?php endwhile; else: ?>
		<p>No matching news available.</p>
	<?php endif; ?>
</div> <!--End content-->
	<?php get_footer(); ?>
