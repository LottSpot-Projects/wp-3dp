<?php 
/*
 * Template Name: Archives
 */

get_header();
get_sidebar(); ?>

<div id="archive">
	<div id="archive-search">
		<?php get_search_form(); ?>
	</div><!--End archive search-->
	
	<div id="monthly-archive">
		<h2>Archive by month:</h2>
		<ul>
			<?php wp_get_archives('type=monthly'); ?>
		</ul>
	</div><!--End monthly archive-->
	
	<div id="category-archive">
		<h2>Archive by category:</h2>
		<ul>
			<?php wp_list_categories(); ?>
		</ul>
	</div><!--End category archive-->
</div><!--End archive-->
