<div id="sidebar">
	<div id="contact-box">
		<h3 id="contact-heading">Contact us</h3>
		<div class="contact-info">
			<h5>Paton Marshall</h5><br />
			<p>Email: <a href="mailto:paton@3drinksproductions.com?subject=3%20Drinks%20Productions%20Inquiry">paton@3drinksproductions.com</a></p>
		</div><!--End of contact info - Paton-->
		<div  class="contact-info">
			<h5>Stephen M. Day</h5><br/ >
			<p>Email: <a href="mailto:steve@3drinksproductions.com?subject=3%20Drinks%20Productions%20Inquiry">steve@3drinksproductions.com</a></p>
		</div><!--End contact info - Steve-->
		<div class="contact-info">
			<h5>John Gonzales</h5><br />
			<p>Email: <a href="mailto:john@3drinksproductions.com?subject=3%20Drinks%20Productions%20Inquiry">john@3drinksproductions.com</a></p>
		</div><!--End contact info - John-->
		<div class="contact-info">
			<h5>Auggie Mares</h5><br />
			<p>Email: <a href="mailto:auggie@3drinksproductions.com?subject=3%20Drinks%20Productions%20Inquiry">auggie@3drinksproductions.com</a></p>
		</div><!--End contact info - Auggie-->
		<div class="contact-info">
			<h5>General Inquiries</h5><br />
			<p><a href="mailto:info@3drinksproductions.com?subject=3%20Drinks%20Productions%20Inquiry">info@3drinksproductions.com</a></p>
		</div><!--End contact info - General Inquiry-->
	</div><!--End contact box-->
	
	<div id="email-box">
		<p>or.. </p><br /><h3 id="email-heading">Email us</h3><br /> <p>for general inqueries!</p>
		<!--<div id="email-inquiry">-->
		  <form action="<?php bloginfo('template_directory'); ?>/email.php" method="post" id="email-form">
		    <p>First Name: <input type="text" size="20" name="fname" /><br />
		    Last Name: <input type="text" size="20" name="lname" /><br />
		    Email address: <input type="text" size="20" name="email" /><br />
		    Phone number: <input type="text" size="14" name="phone" /><br />
		    Message: <textarea name="message" rows="10" columns="25"></textarea><br />
		    <input type="submit" name="send" value="Send" /></p>
		  </form><!--End email form-->
		<!--</div><!--End email inquiry-->
	</div><!--End email box-->
</div><!--End sidebar-->
