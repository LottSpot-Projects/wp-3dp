<?php get_header();
get_sidebar(); ?>



	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="news-post">
			<div class="news-date-author">
				<p><?php the_time('F jS Y'); ?><br />
				by <?php the_author_posts_link(); ?></p>
			</div><!--End news date/author-->
			<div class="news-title">
				<h2><?php the_title(); ?></h2>
			</div><!--End new title-->
			<p class="news-content">
				<?php the_content(); ?>
			</p>
		</div> <!--End content-->
		<?php endwhile; else: ?>
			<p>No matching news available.</p>
		<?php endif; ?>
<?php get_footer(); ?>
