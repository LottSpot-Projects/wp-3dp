<?php 
/*
 * Template Name: News Page
 */
get_header(); ?>

<?php get_sidebar(); 
query_posts('category_name=news&posts_per_page=5') ?>


		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="news-post">
			<div class="news-date-author">
				<p><?php the_time('F jS Y'); ?><br />
				by <?php the_author_posts_link(); ?></p>
			</div><!--End news date/author-->
			<div class="news-title">
				<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			</div><!--End new title-->
			<p class="news-content">
				<?php the_content(); ?>
			</p>
		</div> <!--End content-->
		<?php endwhile; else: ?>
			<p>No matching news available.</p>
		<?php endif; ?>
<?php get_footer(); ?>
