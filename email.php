<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <title>
    Mail sent!
  </title>

  <!--Google web fonts-->
	<link href='http://fonts.googleapis.com/css?family=Economica:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Galdeano' rel='stylesheet' type='text/css'>	
	<link href='http://fonts.googleapis.com/css?family=Economica:400,700,400italic,700italic|Cabin+Condensed|Acme' rel='stylesheet' type='text/css'>
	<!--End Google web fonts-->
	
	<link rel="shortcut icon" href="images/3dpfavicon64.ico" type="image/x-icon" />
	<link rel="stylesheet" href="style.css" type="text/css" />
</head>

<body>

<!--Facebook like button script code-->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<!--End script code-->
	
	<div id="page-wrapper">
		
	<div id="header">
		<div id="header-right-container">
			<div id="social-icons">
				<img src="images/rss_logo.png" alt="Follow 3 Drinks Productions News through RSS" id="button-rss" height="40" width="40" /></a>
				<a href="http://www.facebook.com/3DrinksProductions"><img src="images/facebook_logo.png" alt="3 Drinks Productions on Facebook" id="button-fb" height="40" width="40" /></a>
				<a href="https://twitter.com/3DrinksProds"><img src="images/twitter_logo.png" alt="3 Drinks Procudtions on Twitter" id="button-twitter" height="40" width="40" /></a><br /><!--Send like button to next line-->
				<!--Facebook like button-->
				<div class="fb-like" data-href="http://www.facebook.com/3DrinksProductions?ref=ts" data-send="false" data-layout="box_count" data-width="40" data-show-faces="false" data-colorscheme="dark" data-font="verdana"></div>
				<!--End like button-->
			</div> <!--End social icons-->
		</div> <!--End header right-->
		<img src="images/WatermarkLogo-white.png" alt="3 Drinks Productions" id="header-image" />
	</div> <!--End of header-->

	<div id="page-container">
<div id="content">

<?php 

$_email = 'jlott024@gmail.com';
$_subject = '3 Drinks Productions Inquiry';
$_from = $_POST['email'];
$_fname = $_POST['fname'];
$_lname = $_POST['lname'];
$_phone = $_POST['phone'];
$_content = $_POST['message'];

$_message = "The following message was emailed by " . $_fname . " " . $_lname . ". This individual may be reached by email at " . $_from . " or by phone at " . $_phone . ". The following message was sent:\n" . $_content;

$_message = wordwrap($_message, 70);
mail($_email, $_subject, $_message, "From: " . $_from);

?>
  <h2>The email form is currently not functional.</h2>
  <p>We apologize for the inconvenience. We are still easily reachable by email using the mailto links in our contact box. We hope to have this feature functional very soon.</p>
  <p><a href="/">Return to 3 Drinks Productions</a></p>
</div><!--End content-->
</div><!--End page container-->
</body>
</html>