<?php get_header();
get_sidebar(); ?>

<div id="error-container">
	<h1 id="error-heading">OOPS!</h1>
	<img src="<?php bloginfo('template_directory'); ?>/images/404.png" id="404-image" />
	<p id="error-message">
		That's a 404. Looks like someone had a few too many.
	</p>
</div> <!--end error container-->

<?php get_footer(); ?>