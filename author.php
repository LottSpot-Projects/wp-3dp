<?php get_header();
get_sidebar(); ?>


<?php $_exclude = get_cat_ID('Videos');
query_posts($query_string . "&cat=-$_exclude");
?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div class="news-post">
		<div class="news-date-author">
			<p><?php the_time('F jS Y'); ?><br />
			<?php the_author(); ?></p>
		</div><!--End news date/author-->
		<div class="news-title">
			<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		</div><!--End news title-->
		<div class="news-content">
			<p><?php the_content(); ?></p>
		</div><!--End news content-->
	</div><!--End news post-->
	<?php endwhile; else : ?>
	<p>There were no posts matching your query!</p>
	<?php endif; ?>
	
<?php get_footer(); ?>