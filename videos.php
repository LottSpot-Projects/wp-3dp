<?php 
/*
 * Template Name: Videos Page
 */
get_header(); ?>

<?php get_sidebar(); 
query_posts('category_name=videos');?>

<div id="content">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="video-post">
				<div class="video-title">
					<h2><?php the_title(); ?></h2>
				</div><!--End video title-->
				<?php the_content(); ?>
				
				<div class="bottom-rule"></div><!--End bottom rule-->
			</div><!--End video post-->
		<?php endwhile; else: ?>
			<p>No matching news available.</p>
		<?php endif; ?>
</div> <!--End content-->
	<?php get_footer(); ?>
