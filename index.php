<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head>
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	
	<?php if (is_search()) { ?>
	   <meta name="robots" content="noindex, nofollow" /> 
	<?php } ?>

	<title>
	  3 Drinks Productions
	</title>
	
	<!--Google web fonts-->
	<link href='http://fonts.googleapis.com/css?family=Economica:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Galdeano' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Economica:400,700,400italic,700italic|Cabin+Condensed|Acme' rel='stylesheet' type='text/css'>
	<!--End Google web fonts-->
	
	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/images/3dpfavicon64.ico" type="image/x-icon" />
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" />
</head>

<body>
	
	<!--Facebook like button script code-->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<!--End script code-->
	
	<div id="page-container">
		<div id="watermark-container">
			<a href="<?php echo esc_url(get_permalink(get_page_by_title('News'))); ?>"><img src="<?php bloginfo('template_directory') ?>/images/WatermarkLogo.png" alt="3 Drinks Productions" id="watermark" /></a>
		</div> <!--End of watermark-->
		
		<p id="description-index">
			<?php bloginfo('description'); ?>
		</p>
		
		<div id="social-banners">
			<a href="<?php bloginfo('wpurl') ?>?feed=rss2"><img src="<?php bloginfo('template_directory'); ?>/images/rss_banner_resize.png" alt="Follow 3 Drinks Productions News through RSS" id="banner-rss" /></a>
			<a href="http://www.facebook.com/3DrinksProductions"><img src="<?php bloginfo('template_directory'); ?>/images/facebook_banner_resize.png" alt="3 Drinks Productions on Facebook" id="banner-fb" /></a>
			<a href="https://twitter.com/3DrinksProds"><img src="<?php bloginfo('template_directory'); ?>/images/twitter_banner_resize.png" alt="3 Drinks Procudtions on Twitter" id="banner-twitter" /></a><br /> <!--Send Like button to next line-->
			<!--Facebook like button-->
			<div class="fb-like" data-href="http://www.facebook.com/3DrinksProductions?ref=ts" data-send="false" data-layout="box_count" data-width="40" data-show-faces="false" data-colorscheme="dark" data-font="verdana"></div>
			<!--End like button-->
		</div> <!--End social icons-->
		
		<div id="entrance">
		  <a href="<?php echo esc_url(get_permalink(get_page_by_title('News'))); ?>"><h2>Enter Site</h2></a>
		</div><!--End entrance-->
		
		<div id="on-set-container">
			<a href="<?php echo esc_url(get_permalink(get_page_by_title('News'))); ?>"><img src="<?php bloginfo('template_directory') ?>/images/3drinkset.jpg" alt="3 Drinks Productions" id="on-set" height="200" width="267" /></a>
		</div> <!--End of on set image-->
		
		<div id="toast-container">
			<a href="<?php echo esc_url(get_permalink(get_page_by_title('News'))); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/3drinktoast.jpg" alt="3 Drinks Productions" id="toast" height="200" width="294" /></a>
		</div> <!--End of "toast" image-->
		
		<?php query_posts('category_name=news&posts_per_page=1'); ?>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div id="index-post">
					<p id="index-date-author">
						<?php the_time('F jS Y'); ?><br />
						by <?php the_author_posts_link(); ?></p>
					</p><!--End index date/author-->
				<div id="index-post-title">
					<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
				</div><!--End index post title-->
				
				<div id="index-content">
					<p><?php the_content(); ?></p>
				</div> <!--End of index content-->
			</div> <!--End of index News-->
		<?php endwhile; else: ?>
			<p class="search-response">No matching news available.</p>
		<?php endif; ?>

		<?php get_footer(); ?>
